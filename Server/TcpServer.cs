using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TcpLib
{
    public class TcpServer
    {
        private int _port;
        private Socket _listener;
        private TcpServiceProvider _provider;

        internal ArrayList _connections;
        private int _maxConnections = 100;

        private AsyncCallback ConnectionReady;
        private WaitCallback AcceptConnection;
        private AsyncCallback ReceivedDataReady;

        /// <SUMMARY>
        /// Initializes server. To start accepting connections call Start method.
        /// </SUMMARY>
        public TcpServer(TcpServiceProvider provider, int port)
        {
            this._provider = provider;
            this._port = port;
            this._listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream,
                    ProtocolType.Tcp);
            this._connections = new ArrayList();
            this.ConnectionReady = new AsyncCallback(this.ConnectionReady_Handler);
            this.AcceptConnection = new WaitCallback(this.AcceptConnection_Handler);
            this.ReceivedDataReady = new AsyncCallback(this.ReceivedDataReady_Handler);
        }


        /// <SUMMARY>
        /// Start accepting connections.
        /// A false return value tell you that the port is not available.
        /// </SUMMARY>
        public bool Start()
        {
            try
            {
                IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 15555);

                //IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), this._port);
                this._listener.Bind(localEndPoint);
                this._listener.Listen(100);
                this._listener.BeginAccept(this.ConnectionReady, null);
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <SUMMARY>
        /// Callback function: A new connection is waiting.
        /// </SUMMARY>
        private void ConnectionReady_Handler(IAsyncResult ar)
        {
            lock(this)
            {
                if(this._listener == null) return;
                Socket conn = this._listener.EndAccept(ar);
                if(this._connections.Count >= this._maxConnections)
                {
                    //Max number of connections reached.
                    string msg = "SE001: Server busy";
                    conn.Send(Encoding.UTF8.GetBytes(msg), 0, msg.Length, SocketFlags.None);
                    conn.Shutdown(SocketShutdown.Both);
                    conn.Close();
                }
                else
                {
                    //Start servicing a new connection
                    ConnectionState st = new ConnectionState();
                    st._conn = conn;
                    st._server = this;
                    st._provider = (TcpServiceProvider) this._provider.Clone();
                    st._buffer = new byte[4];
                    this._connections.Add(st);
                    //Queue the rest of the job to be executed latter
                    ThreadPool.QueueUserWorkItem(this.AcceptConnection, st);
                }
                //Resume the listening callback loop
                this._listener.BeginAccept(this.ConnectionReady, null);
            }
        }


        /// <SUMMARY>
        /// Executes OnAcceptConnection method from the service provider.
        /// </SUMMARY>
        private void AcceptConnection_Handler(object state)
        {
            ConnectionState st = state as ConnectionState;
            try{ st._provider.OnAcceptConnection(st); }
            catch {
                //report error in provider... Probably to the EventLog
            }
            //Starts the ReceiveData callback loop
            if(st._conn.Connected)
                st._conn.BeginReceive(st._buffer, 0, 0, SocketFlags.None,
                        this.ReceivedDataReady, st);
        }


        /// <SUMMARY>
        /// Executes OnReceiveData method from the service provider.
        /// </SUMMARY>
        private void ReceivedDataReady_Handler(IAsyncResult ar)
        {
            ConnectionState st = ar.AsyncState as ConnectionState;
            try
            {
                st._conn.EndReceive(ar);
                //Im considering the following condition as a signal that the
                //remote host droped the connection.
                if (st._conn.Available == 0)
                    this.DropConnection(st);
                else
                {
                    try
                    {
                        st._provider.OnReceiveData(st);
                    }
                    catch
                    {
                        //report error in the provider
                    }
                    //Resume ReceivedData callback loop
                    if (st._conn.Connected)
                        st._conn.BeginReceive(st._buffer, 0, 0, SocketFlags.None, this.ReceivedDataReady, st);
                }
            }
            catch
            {
                if (!st.Connected)
                    this.DropConnection(st);
            }
        }


        /// <SUMMARY>
        /// Shutsdown the server
        /// </SUMMARY>
        public void Stop()
        {
            lock(this)
            {
                this._listener.Close();
                this._listener = null;
                //Close all active connections
                foreach(object obj in this._connections)
                {
                    ConnectionState st = obj as ConnectionState;
                    try{ st._provider.OnDropConnection(st);    }
                    catch{
                        //some error in the provider
                    }
                    st._conn.Shutdown(SocketShutdown.Both);
                    st._conn.Close();
                }
                this._connections.Clear();
            }
        }


        /// <SUMMARY>
        /// Removes a connection from the list
        /// </SUMMARY>
        internal void DropConnection(ConnectionState st)
        {
            lock(this)
            {
                if (st.Connected)
                {
                    st._conn.Shutdown(SocketShutdown.Both);
                    st._conn.Close();
                }
                if(this._connections.Contains(st))
                    this._connections.Remove(st);
            }
        }


        public int MaxConnections
        {
            get
            {
                return this._maxConnections;
            }
            set
            {
                this._maxConnections = value;
            }
        }


        public int CurrentConnections
        {
            get
            {
                lock(this){ return this._connections.Count; }
            }
        }
    }
}