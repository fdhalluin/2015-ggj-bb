using System;
using System.Net.Sockets;

using NetworkLib;

using TcpLib;

namespace TcpServerDemo
{
	/// <SUMMARY>
	/// BroadcastServiceProvider. Just replies messages received from the clients.
	/// </SUMMARY>
	public class BroadcastServiceProvider: TcpServiceProvider
	{
	    private byte[] receivedData = new byte[0];

	    private int bufferOffset = 0;

	    private int packageSizeRemaining = 0;

		public override object Clone()
		{
			return new BroadcastServiceProvider();
		}

		public override void OnAcceptConnection(ConnectionState state)
		{
			string message = "Hello! Connection established.";
		    NetworkPackage networkPackage = new NetworkPackage{Comment = message};
		    var data = NetworkPackage.BuildHeaderPackage(networkPackage);

		    if (!state.Write(data, 0, data.Length))
				state.EndConnection(); //if write fails... then close connection
		}

	    public override void OnReceiveData(ConnectionState state)
		{
			while(state.AvailableData > 0)
			{
			    if (packageSizeRemaining == 0)
			    {
                    // Read new package length.
                    byte[] pSize = new byte[4];
			        state.Read(pSize, 0, 4);
			        this.packageSizeRemaining = BitConverter.ToInt32(pSize, 0);
                    this.receivedData = new byte[this.packageSizeRemaining];
			        this.bufferOffset = 0;
			    }

			    int readBytes = state.Read(this.receivedData, this.bufferOffset, this.packageSizeRemaining - this.bufferOffset);
			    this.bufferOffset += readBytes;
			    this.packageSizeRemaining -= readBytes;
				if(this.packageSizeRemaining <= 0)
				{
				    this.Broadcast(state);
				}
				else state.EndConnection(); //If read fails then close connection
			}
		}


        /// <SUMMARY>
        /// Broadcast message.
        /// </SUMMARY>
        private void Broadcast(ConnectionState conn)
        {
            for (int index = conn._server._connections.Count - 1; index >= 0; index--)
            {
                ConnectionState obj = (ConnectionState)conn._server._connections[index];
                if (obj == conn)
                    continue;

                try
                {
                    var data = NetworkPackage.BuildHeaderPackage(this.receivedData);
                    obj.Write(data, 0, data.Length);
                }
                catch
                {
                    //some error in the provider
                }
            }
        }

	    public override void OnDropConnection(ConnectionState state)
		{
			//Nothing to clean here
		}
	}
}
