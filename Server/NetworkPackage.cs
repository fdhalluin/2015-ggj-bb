using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace TcpServerLib
{
    [Serializable]
    public struct NetworkPackage
    {
        public int NetworkID { get; set; }
        public string Comment { get; set; }
        public object Data { get; set; }

        // Convert an object to a byte array
        public byte[] ObjectToByteArray()
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, this);
            return ms.ToArray();
        }

        // Convert a byte array to an Object
        public static NetworkPackage ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            NetworkPackage obj = (NetworkPackage)binForm.Deserialize(memStream);
            return obj;
        }
    }
}