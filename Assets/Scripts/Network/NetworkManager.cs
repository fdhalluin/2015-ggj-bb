using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Assets.Scripts.Network.Packages;

using NetworkLib;

using UnityEngine;

using Debug = UnityEngine.Debug;

public class NetworkManager : MonoBehaviour
{
    public string ServerIP = "127.0.0.1";

    public int ServerPort = 15555;

    private TcpClient mySocket;

    private Boolean socketReady;

    private NetworkStream stream;

    private int packageSizeRemaining;

    private byte[] receivedData;

    private int bufferOffset;

    private readonly List<INetworkable> networkUnits = new List<INetworkable>();

    private Queue<NetworkPackage> delayedPackages = new Queue<NetworkPackage>();

    private float deltaTime;

    public static NetworkManager Instance { get; private set; }

    public void OnEnable()
    {
        Instance = this;
    }

    private void ProcessDelayedPackages()
    {
        while (this.delayedPackages.Count > 0)
        {
            var p = delayedPackages.Dequeue();
            this.Broadcast(p);
        }
    }

    public void Broadcast(INetworkable sender, string comment, object data)
    {
        var p = new NetworkPackage {Timestamp = DateTime.UtcNow.Ticks, Data = data, Comment = comment, NetworkID = sender.NetworkID};
        if (!this.socketReady)
        {
            this.delayedPackages.Enqueue(p);
            return;
        }
        
        this.Broadcast(p);
    }

    private void Broadcast(NetworkPackage inPackage)
    {
        if ((DateTime.UtcNow.Ticks - inPackage.Timestamp) / (double)TimeSpan.TicksPerSecond > 0.5)
        {
            Debug.LogWarning("Trying to send an outdated package.");
        }

        byte[] buildHeaderPackage = NetworkPackage.BuildHeaderPackage(inPackage);
        try
        {
            this.stream.Write(buildHeaderPackage, 0, buildHeaderPackage.Length);
            this.stream.Flush();
        }
        catch {}
    }

    public bool Init()
    {
        try
        {
            this.mySocket = new TcpClient(this.ServerIP, this.ServerPort);
            this.stream = this.mySocket.GetStream();
            this.stream.ReadTimeout = 1;
            this.stream.WriteTimeout = 10;
            
            this.socketReady = true;

            this.StartCoroutine(UpdateLoop());

            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("Socket error: " + e);
            return false;
        }
    }

    private IEnumerator UpdateLoop()
    {
        this.ProcessDelayedPackages();
        WaitForSeconds waitForSeconds = new WaitForSeconds(0.1f);
        bool wait = true;
            
        while (true)
        {
            if (wait)
            {
                wait = false;
                yield return waitForSeconds;
            }

            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            if (this.packageSizeRemaining == 0)
            {
                // Read new package length.
                if (this.receivedData == null)
                    this.receivedData = new byte[4];

                int read;
                //Stopwatch sr1 = new Stopwatch();
                try
                {
                    //if (!this.stream.DataAvailable)
                    //{
                    //    wait = true;
                    //    continue;
                    //}

                    //sr1.Start();
                    read = this.stream.Read(this.receivedData,
                            this.bufferOffset,
                            this.receivedData.Length - this.bufferOffset);
                }
                catch (Exception)
                {
                    //sr1.Stop();
                    //Debug.Log("Read: " + sr1);
                    wait = true;
                    continue;
                }
                finally
                {
                    //sr1.Stop();
                    //Debug.Log("Read: " + sr1);
                }


                this.bufferOffset += read;
                if (this.bufferOffset < 4)
                    continue;

                this.packageSizeRemaining = BitConverter.ToInt32(this.receivedData, 0);
                this.receivedData = new byte[this.packageSizeRemaining];
                this.bufferOffset = 0;
            }

            int readBytes;
            //Stopwatch sr = new Stopwatch();
            try
            {
                //if (!this.stream.DataAvailable)
                //{
                //    wait = true;
                //    continue;
                //}

                //sr.Start();
                readBytes = this.stream.Read(this.receivedData,
                        this.bufferOffset,
                        this.packageSizeRemaining - this.bufferOffset);
            }
            catch (Exception)
            {
                //sr.Stop();
                //Debug.Log("Read: " + sr);
                wait = true;
                continue;
            }
            finally
            {
                //sr.Stop();
                //Debug.Log("Read: " + sr);
            }

            if (readBytes == 0)
                continue;

            this.bufferOffset += readBytes;
            this.packageSizeRemaining -= readBytes;
            if (this.packageSizeRemaining <= 0)
            {
                this.ProcessMessage();
                this.receivedData = null;
                this.bufferOffset = 0;
                this.packageSizeRemaining = 0;
            }

            //sw.Stop();
            //Debug.Log("Nettime: " + sw.Elapsed);
        }
    }


    private void ProcessMessage()
    {
        var inPackage = NetworkPackage.GetPackage(this.receivedData);

        if (inPackage.Comment != "Position")
            Debug.Log("Receiving: " +  inPackage);

        double d = (DateTime.UtcNow.Ticks - inPackage.Timestamp) / (double)TimeSpan.TicksPerSecond;
        if (d > 0.5)
        {
            Debug.LogWarning("Received package is late. d=" + d);
        }

        for (int index = this.networkUnits.Count - 1; index >= 0; index--)
            if (this.networkUnits[index].NetworkID == inPackage.NetworkID)
                this.networkUnits[index].OnReceived(inPackage);
    }

    public static MemoryStream SerializeToStream(object o)
    {
        MemoryStream stream = new MemoryStream();
        IFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, o);
        return stream;
    }

    public static object DeserializeFromStream(Stream stream)
    {
        IFormatter formatter = new BinaryFormatter();
        //stream.Seek(0, SeekOrigin.Begin);
        object o = formatter.Deserialize(stream);
        return o;
    }

    public void CloseSocket()
    {
        if (!this.socketReady)
            return;
        this.stream.Close();
        this.mySocket.Close();
        this.socketReady = false;
    }

    public void Register(INetworkable networkable)
    {
        Debug.Log("Registering updates for " + networkable);
        this.networkUnits.Add(networkable);
    }

    public void UnRegister(INetworkable networkable)
    {
        Debug.Log("UnRegistering updates for " + networkable);
        this.networkUnits.Remove(networkable);
    }
}