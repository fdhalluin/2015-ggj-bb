using System;

namespace Assets.Scripts.Network.Packages
{
    [Serializable]
    internal class JoinData {
        public JoinData(int pID,
                bool isObserverView)
        {
            this.Pid = pID;
            this.IsObserverView = isObserverView;
        }

        public override string ToString()
        {
            return string.Format("Pid: {0}, IsObserverView: {1}", this.Pid, this.IsObserverView);
        }

        public int Pid { get;  set; }

        public bool IsObserverView { get;  set; }
    }
}