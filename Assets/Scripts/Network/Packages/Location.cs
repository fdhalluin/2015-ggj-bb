﻿using System;

using UnityEngine;

namespace Assets.Scripts.Network.Packages
{
    [Serializable]
    public struct Location
    {
        private float oriW;

        private float oriZ;

        private float oriY;

        private float oriX;

        private float posZ;

        private float posY;

        private float posX;

        private float speed;

        private bool pushing;

        public Location(Vector3 position,
                Quaternion orientation, float speed, bool pushing)
        {
            this.posX = position.x;
            this.posY = position.y;
            this.posZ = position.z;
            this.oriX = orientation.x;
            this.oriY = orientation.y;
            this.oriZ = orientation.z;
            this.oriW = orientation.w;
            this.speed = speed;
            this.pushing = pushing;
        }

        public float OriW { get { return this.oriW; } set { this.oriW = value; } }

        public float OriZ { get { return this.oriZ; } set { this.oriZ = value; } }

        public float OriY { get { return this.oriY; } set { this.oriY = value; } }

        public float OriX { get { return this.oriX; } set { this.oriX = value; } }

        public float PosZ { get { return this.posZ; } set { this.posZ = value; } }

        public float PosY { get { return this.posY; } set { this.posY = value; } }

        public float PosX { get { return this.posX; } set { this.posX = value; } }

        public float Speed { get { return this.speed; } set { this.speed = value; } }

        public bool Pushing { get { return this.pushing; } set { this.pushing = value; } }

        public void ApplyTo(Transform transform)
        {
            transform.position = new Vector3(PosX, PosY, PosZ);
            transform.rotation = new Quaternion(OriX, OriY, OriZ, OriW);
        }

        public static void ApplyLerped(Location location,
                Location locationTo,
                double progress, Transform transform)
        {
            float p = Mathf.Clamp01((float)progress);
            
            transform.position = new Vector3(Mathf.Lerp(location.PosX, locationTo.PosX, p),
                    Mathf.Lerp(location.PosY, locationTo.PosY, p),
                    Mathf.Lerp(location.PosZ, locationTo.PosZ, p));
            
            transform.rotation = new Quaternion(Mathf.Lerp(location.OriX, locationTo.OriX, p),
                    Mathf.Lerp(location.OriY, locationTo.OriY, p),
                    Mathf.Lerp(location.OriZ, locationTo.OriZ, p),
                    Mathf.Lerp(location.OriW, locationTo.OriW, p));
        }
    }
}