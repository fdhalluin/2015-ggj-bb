using NetworkLib;

namespace Assets.Scripts.Network.Packages
{
    public struct KeepAlive : INetworkable {
        public int NetworkID { get; private set; }

        public void OnReceived(NetworkPackage inPackage)
        {
        }
    }
}