using NetworkLib;

public interface INetworkable {
    int NetworkID { get; }

    void OnReceived(NetworkPackage inPackage);
}