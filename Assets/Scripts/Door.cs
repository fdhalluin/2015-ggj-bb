﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Door : MonoBehaviour
{
	public bool Open = false;

	public GameObject Panel;

	public Vector3 OpenOffset = new Vector3(0, -3, 0);
	public float Damping = 0.1f;

	public Material Material;
	public MeshRenderer Marker;
	public MeshRenderer PanelRenderer;
	public int PanelRendererMaterialId = 1;

	public List<Switch> Switches = new List<Switch>();

	private Material _lastMaterial;
	
	void Update()
	{
		if (Panel == null)
			return;

		var dt = Time.deltaTime;
		Vector3 targetPanelPosition = Open ? OpenOffset : Vector3.zero;
		Open = false;

		if (!Application.isPlaying)
		{
			Panel.transform.localPosition = targetPanelPosition;
		}
		else
		{
			Panel.transform.localPosition = Utils.Damp(Panel.transform.localPosition, targetPanelPosition, Damping, dt);
		}

		if (!Application.isPlaying)
		{
			UpdateSwitches();
			UpdateMaterial();
		}
	}

	void UpdateSwitches()
	{
		for (int i = 0; i < Switches.Count; ++i)
		{
			var doorSwitch = Switches[i];
			if (doorSwitch == null || doorSwitch.Door != this)
			{
				Switches.RemoveAt(i);
				--i;
			}
		}
	}

	public void UpdateMaterial()
	{
		_lastMaterial = Material;
		Marker.sharedMaterial = Material;
		for (int i = 0; i < Switches.Count; ++i)
		{
			Switches[i].Material = Material;
			Switches[i].UpdateMaterial();
		}
		var sharedMaterials = PanelRenderer.sharedMaterials;
		sharedMaterials[PanelRendererMaterialId] = Material;
		PanelRenderer.sharedMaterials = sharedMaterials;
	}
}
