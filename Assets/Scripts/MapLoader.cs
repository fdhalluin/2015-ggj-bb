﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class MapLoader : MonoBehaviour
{
	public TextAsset MapFile;

	public Transform TileContainer;
	public Transform PropContainer;
	public Transform DoorContainer;
	public Transform SwitchContainer;

	public List<Tile> TileTemplates = new List<Tile>();
	public List<Prop> PropTemplates = new List<Prop>();
	public Door DoorTemplate;
	public Switch SwitchTemplate;

	public List<Tile> Tiles = new List<Tile>();
	public List<Prop> Props = new List<Prop>();
	public List<Door> Doors = new List<Door>();
	public List<Switch> Switches = new List<Switch>();

	public Vector3 LocalRotation;
	public float LocalScale;
	public float Offset;

	public bool AskReset = false;

	public enum EObjectType
	{
		Tile = 1,
		Prop = 2,
		Door = 3,
		Switch = 4,
	}

	// Use this for initialization
	void Start()
	{
		AskReset = false;
	}
	
	// Update is called once per frame
	void Update()
	{
		if (AskReset)
		{
			AskReset = false;
			Reset();
		}
	}

	void Reset()
	{
		var tiles = GetComponentsInChildren<Tile>();
		for (int i = 0; i < tiles.Length; ++i)
		{
			DestroyImmediate(tiles[i].gameObject);
		}
		Tiles.Clear();

		var props = GetComponentsInChildren<Prop>();
		for (int i = 0; i < props.Length; ++i)
		{
			DestroyImmediate(props[i].gameObject);
		}
		Props.Clear();
		
		var doors = GetComponentsInChildren<Door>();
		for (int i = 0; i < doors.Length; ++i)
		{
			DestroyImmediate(doors[i].gameObject);
		}
		Doors.Clear();
		
		var switches = GetComponentsInChildren<Switch>();
		for (int i = 0; i < switches.Length; ++i)
		{
			DestroyImmediate(switches[i].gameObject);
		}
		Switches.Clear();
		
		var text = MapFile.text;
		string[] lines = text.Split ('\n');
		foreach (string line in lines)
		{
			string[] values = line.Split('\t');
			float x = float.Parse(values[0]);
			float z = float.Parse(values[1]);
			float r = float.Parse(values[2]);
			int id = int.Parse(values[3]);

			EObjectType type = (EObjectType) int.Parse(values[4]);

			switch (type)
			{
				case EObjectType.Tile:
					SpawnTile(x, z, r, id);
					break;
				case EObjectType.Prop:
					SpawnProp(x, z, r, id);
					break;
				case EObjectType.Door:
				print ("spawning door");
					SpawnDoor(x, z, r, id);
					break;
				case EObjectType.Switch:
					SpawnSwitch(x, z, r, id);
					break;
			}

		}
	}

	void SpawnTile(float x, float z, float r, int id)
	{
		var template = TileTemplates[id];
#if UNITY_EDITOR
		var tileObject = PrefabUtility.InstantiatePrefab(template.gameObject) as GameObject;
#else
		var tileObject = Instantiate(template.gameObject) as GameObject;
#endif
		var tileTransform = tileObject.transform;
		tileObject.name = template.name + "_" + Mathf.RoundToInt(x) + "_" + Mathf.RoundToInt(x);
		tileTransform.parent = TileContainer;
		tileTransform.localScale = Vector3.one * LocalScale;
		tileTransform.localRotation	= Quaternion.Euler(LocalRotation.x, LocalRotation.y + r, LocalRotation.z);
		tileTransform.localPosition = new Vector3(x, 0, z) * Offset;
		var tile = tileObject.GetComponent<Tile>();
		tile.X = Mathf.RoundToInt(x);
		tile.Y = Mathf.RoundToInt(z);
		Tiles.Add(tile);
	}

	void SpawnProp(float x, float z, float r, int id)
	{
		var template = PropTemplates[id];
#if UNITY_EDITOR
		var propObject = PrefabUtility.InstantiatePrefab(template.gameObject) as GameObject;
#else
		var propObject = Instantiate(template.gameObject) as GameObject;
#endif
		var propTransform = propObject.transform;
		propObject.name = template.name + "_" + Mathf.RoundToInt(x) + "_" + Mathf.RoundToInt(x);
		propTransform.parent = PropContainer;
		propTransform.localScale = Vector3.one * LocalScale;
		propTransform.localRotation	= Quaternion.Euler(LocalRotation.x, LocalRotation.y + r, LocalRotation.z);
		propTransform.localPosition = new Vector3(x, 0, z) * Offset;
		var prop = propObject.GetComponent<Prop>();
		Props.Add(prop);
	}

	void SpawnDoor(float x, float z, float r, int id)
	{
		var template = DoorTemplate;
#if UNITY_EDITOR
		var doorObject = PrefabUtility.InstantiatePrefab(template.gameObject) as GameObject;
#else
		var doorObject = Instantiate(template.gameObject) as GameObject;
#endif
		var doorTransform = doorObject.transform;
		doorObject.name = template.name + "_" + Mathf.RoundToInt(x) + "_" + Mathf.RoundToInt(x);
		doorTransform.parent = DoorContainer;
		doorTransform.localScale = Vector3.one * LocalScale;
		doorTransform.localRotation	= Quaternion.Euler(LocalRotation.x, LocalRotation.y + r, LocalRotation.z);
		doorTransform.localPosition = new Vector3(x, 0, z) * Offset;
		var door = doorObject.GetComponent<Door>();
		Doors.Add(door);
	}

	void SpawnSwitch(float x, float z, float r, int id)
	{
	}
}
