﻿using UnityEngine;
using System.Collections;

public static class Layers
{
	public static int SwitchLayer = 8;
	public static int SwitchLayerMask = 1 << 8;
	public static int DefaultLayer = 0;
	public static int DefaultLayerMask = 1 << 0;
	public static int TopViewOnlyLayer = 9;
	public static int TopViewOnlyLayerMask = 1 << 9;
	public static int PlayerViewOnlyLayer = 10;
	public static int PlayerViewOnlyLayerMask = 1 << 10;
	public static int PropLayer = 11;
	public static int PropLayerMask = 1 << 11;
	public static int SafeLayer = 12;
	public static int SafeLayerMask = 1 << 12;

	public static int InteractableLayerMask = SwitchLayerMask | SafeLayerMask | PropLayerMask;
}
