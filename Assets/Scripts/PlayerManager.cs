﻿using System;
using System.Collections.Generic;

using Assets.Scripts.Network.Packages;

using NetworkLib;

using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class PlayerManager : MonoBehaviour, INetworkable
{
	public GameObject UI;

    public Text Notification;

    public Player[] Players;
    public GameObject[] PlayerSlitScreens;

    public GameObject ObserverMap;

    public bool ShowObserverMap = false;

    public int LocalPlayerCount = 2;

    public int TotalPlayerCount = 2;

    private int nextFreePlayerSlot = 1;

    private int uniqueID = Guid.NewGuid().GetHashCode();

    private readonly List<JoinData> joinedPlayers = new List<JoinData>();

    public bool IsMaster { get; private set; }

    private bool isIngame;

    
    public void OnEnable()
    {
        Instance = this;
    }

	public void Start()
	{
		Reset();

		if (UI != null)
			UI.SetActive(true);
		else
			Init();
	}

	public void Reset()
	{
		for (int i = 0; i < Players.Length; ++i)
		{
			Players[i].gameObject.SetActive(false);
		}
		for (int i = 0; i < PlayerSlitScreens.Length; ++i)
			PlayerSlitScreens[i].gameObject.SetActive(false);
	}

    // Use this for initialization
	public void Init()
	{
        NetworkManager.Instance.Register(this);

        // Create local players
	    for (int index = 0; index < LocalPlayerCount; index++)
	    {
            int pID = this.uniqueID + index;
            JoinData joinData = new JoinData(pID, index == LocalPlayerCount -1 && ShowObserverMap);
            NetworkManager.Instance.Broadcast(this, "Join", joinData);
            this.Join(joinData);	        
	    }
	}

    public int NetworkID { get { return "PlayerManager".GetHashCode(); } }

    public static PlayerManager Instance { get; private set; }

    public void OnReceived(NetworkPackage inPackage)
    {
        switch (inPackage.Comment)
        {
            case "Join":
            {
                if (this.isIngame)
                {
                    Debug.LogWarning("Player tried to join running game. Ignoring it");
                    break;
                }

                JoinData data = (JoinData)inPackage.Data;
                this.Join(data);

                break;
            }

            case "ListUpdate":
            {
                if (this.isIngame)
                {
                    Debug.LogWarning("New list update while running. Ignoring.");
                    break;
                }

                List<JoinData> playerList = (List<JoinData>)inPackage.Data;
                this.UpdateList(playerList);

                break;
            }
            case "StartGame":
            {

                List<JoinData> playerSetup = (List<JoinData>)inPackage.Data;
                this.StartGame(playerSetup);

                break;
            }
        }

    }

    private void StartGame(IEnumerable<JoinData> playerSetup)
    {
		if (Notification != null)
            Notification.text = "Starting game...";

        this.joinedPlayers.Clear();
        this.joinedPlayers.AddRange(playerSetup);

        if (this.ShowObserverMap)
            this.ObserverMap.SetActive(true);

        int layoutIndex = this.LocalPlayerCount - 1;
        if (ShowObserverMap)
            layoutIndex--;

        GameObject playerSlitScreen = layoutIndex >= 0 ? this.PlayerSlitScreens[layoutIndex] : null;
        var cams = playerSlitScreen != null ? playerSlitScreen.GetComponentsInChildren<PlayerCamera>(true) : null;

        if (cams != null && cams.Length != layoutIndex + 1)
            throw new ArgumentException("Split screen layout does not match local player count");

        for (int index = 0; index < this.joinedPlayers.Count; index++)
        {
            var joinedPlayer = this.joinedPlayers[index];
            bool isOwned = false;

            for (int id = 0; id < this.LocalPlayerCount; id++)
            {
                if (joinedPlayer.IsObserverView)
                    continue;

                if (joinedPlayer.Pid != this.uniqueID + id)
                    continue;
                
                isOwned = true;
                    
                if (index < this.Players.Length)
                    this.Players[index].ControllerID = id + 1;

                if (cams == null || id >= cams.Length || index >= this.Players.Length)
                    continue;
                    
                cams[id].Player = this.Players[index];

                if (!this.ShowObserverMap)
                    continue;
                    
                Rect rect = cams[id].camera.rect;
                rect.x = rect.x / 2 + 0.5f;
                rect.width /= 2f;
                cams[id].camera.rect = rect;
            }

            if (index < this.Players.Length)
            {
                this.Players[index].IsOwned = isOwned;
                this.Players[index].NetworkID = joinedPlayer.Pid;
            }

            Debug.Log(string.Format("Assigning player {0} to id {1} (Owned={2})", joinedPlayer, index + 1, isOwned));
        }

        if (playerSlitScreen != null)
        {
            playerSlitScreen.gameObject.SetActive(true);
        }
        else
        {
            this.ObserverMap.GetComponentInChildren<Camera>()
                    .rect = new Rect(0, 0, 1, 1);
        }

        for (int i = 0; i < this.TotalPlayerCount -1; i++)
        {
            Debug.Log("Activating player " + i);
            this.Players[i].gameObject.SetActive(true);
        }

		if (Notification != null)
            Notification.text = string.Empty;
    }

    private void UpdateList(IEnumerable<JoinData> playerList)
    {
        foreach (JoinData player in playerList)
        {
            if (this.joinedPlayers.Find(x=>x.Pid == player.Pid) == null)
                this.joinedPlayers.Add(player);
        }

        Debug.Log("Player list consists of: ");
        foreach (JoinData joinedPlayer in this.joinedPlayers)
            Debug.Log("x " + joinedPlayer);

        if (this.joinedPlayers.Count == this.TotalPlayerCount)
            this.InitGame();
    }

    private void Join(JoinData data)
    {
        this.joinedPlayers.Add(data);
        int waitingFor = TotalPlayerCount - this.joinedPlayers.Count;
		if (Notification != null)
            Notification.text = "Player joined. Waiting for " + waitingFor + " more players.";
        NetworkManager.Instance.Broadcast(this, "ListUpdate", this.joinedPlayers);

        Debug.Log("Got join of " + data);

        this.UpdateList(this.joinedPlayers);
    }

    private void InitGame()
    {
        this.isIngame = true;

        joinedPlayers.Sort((a, b) =>
        {
            int compareTo = a.IsObserverView.CompareTo(b.IsObserverView);
            return compareTo == 0 ? a.Pid.CompareTo(b.Pid) : compareTo;
        });

        this.IsMaster = this.joinedPlayers[0].Pid == uniqueID;

        Debug.Log("Is master client: " + this.IsMaster);

        if (this.IsMaster)
        {
            NetworkManager.Instance.Broadcast(this, "StartGame", this.joinedPlayers);
            this.StartGame(new List<JoinData>(this.joinedPlayers));
        }
    }
}