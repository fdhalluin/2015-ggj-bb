﻿using UnityEngine;
using System.Collections;

public static class Utils
{
	public static float Damp(float source, float target, float factor, float dt, float speed = 1)
	{
		float lerpFactor = dt * speed < float.Epsilon ? 0 : (dt * speed) / (dt * speed + factor);
		return Mathf.Lerp(source, target, lerpFactor);
	}

	public static Color Damp(Color source, Color target, float factor, float dt, float speed = 1)
	{
		float lerpFactor = dt * speed < float.Epsilon ? 0 : (dt * speed) / (dt * speed + factor);
		return Color.Lerp(source, target, lerpFactor);
	}
	
	public static Vector3 Damp(Vector3 source, Vector3 target, float factor, float dt, float speed = 1)
	{
		float lerpFactor = dt * speed < float.Epsilon ? 0 : (dt * speed) / (dt * speed + factor);
		return Vector3.Lerp(source, target, lerpFactor);
	}
	
	public static Quaternion Damp(Quaternion source, Quaternion target, float factor, float dt, float speed = 1)
	{
		float lerpFactor = dt * speed < float.Epsilon ? 0 : (dt * speed) / (dt * speed + factor);
		return Quaternion.Slerp(source, target, lerpFactor);
	}

	public static Vector3 CubicBezier(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
	{
		return Mathf.Pow(1 - t, 3) * p0 + 3 * Mathf.Pow(1 - t, 2) * t * p1 + 3 * (1 - t) * Mathf.Pow(t, 2) * p2 + Mathf.Pow(t, 3) * p3;
	}
}
