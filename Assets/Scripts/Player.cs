﻿using System;

using Assets.Scripts.Network.Packages;

using NetworkLib;

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Player : MonoBehaviour, INetworkable
{
    private bool isOwned = true;

	public string MoveXAxis = "P1_MoveX";
	public string MoveZAxis = "P1_MoveZ";
	public string LookXAxis = "P1_LookX";
	public string LookZAxis = "P1_LookZ";
	public string ActionButton = "P1_Action";
	public string ToggleLightButton = "P1_ToggleLight";

	public float MoveSpeed = 3;
	public float SnapDistance = 1f;
	public float SnapDamping = 0.1f;
	public float LookDamping = 0.15f;

	public float MaxVelocityChange = 1;

	public bool MouseLook = false;

	public Vector3 Velocity = Vector3.zero;

	public Camera Camera;

	public Switch Switch;
	public Prop Prop;
	public Safe Safe;

	public GameObject Selection;

	public float SelectionRadius = 2.5f;
	public float SelectionAngle = 60;

	public GameObject Hand;
	public float HandOffset = 0.2f;

	public GameObject FlashLight;

	public Vector3 MoveInput;
	public Vector3 LookInput;

    private bool pushing = false;

	public float BaseHeight = 0.9f;

	public float VelocityThreshold = 0.2f;

	public bool HasKey = false;
	public bool HasDiamonds = false;
	public bool HasEscaped = false;

	public Material Material;
	public Animator Animator;
	public Light Light;
	public SkinnedMeshRenderer Vest;
	public SkinnedMeshRenderer Pants;

    private int uniqueID = Guid.NewGuid().GetHashCode();
    private double deltaTime;

    [SerializeField]
    private int controllerID;

    private Vector3 targetPos;

    private Vector3 targetOri;

    private float Speed;

    // Use this for initialization
	void Start()
	{
        if (!this.isOwned)
	        NetworkManager.Instance.Register(this);

		if (Camera == null)
			Camera = GetComponentInChildren<Camera>();

		if (Animator == null)
			Animator = GetComponentInChildren<Animator>();

		UpdateMaterial();

		this.UpdateInputControls();
	}

    private void UpdateInputControls()
    {
        this.MoveXAxis = string.Format("P{0}_MoveX", this.ControllerID);
        this.MoveZAxis = string.Format("P{0}_MoveZ", this.ControllerID);
        this.LookXAxis = string.Format("P{0}_LookX", this.ControllerID);
        this.LookZAxis = string.Format("P{0}_LookZ", this.ControllerID);
        this.ActionButton = string.Format("P{0}_Action", this.ControllerID);
        this.ToggleLightButton = string.Format("P{0}_ToggleLight", this.ControllerID);
    }

    void OnDrawGizmos()
	{
		if (Switch == null)
			return;

		var basePosition = transform.position + Vector3.up * BaseHeight;
		var offset = Switch.transform.position - basePosition;
		var distance = offset.magnitude;
		var direction = offset.normalized;

		Gizmos.color = Color.green;
		Gizmos.DrawLine(basePosition, Switch.transform.position);

		RaycastHit hit;
		if (Physics.Raycast(basePosition, direction, out hit, distance - HandOffset,
		                    Layers.InteractableLayerMask | Layers.DefaultLayerMask))
		{
			Gizmos.color = Color.green;
			Gizmos.DrawLine(basePosition, hit.point);
			Gizmos.DrawSphere(hit.point, 0.1f);
		}
	}

	void UpdateAnimation()
	{
		if (Animator == null)
			return;
		Animator.SetFloat("Speed", Speed);
		Animator.SetBool("Pushing", Pushing);
	}

	void UpdateMaterial()
	{
		if (Material == null)
			return;

		Light.color = Material.color;
		if (Vest != null)
		{
			var materials = Vest.sharedMaterials;
			materials[0] = Material;
			Vest.sharedMaterials = materials;
		}
		if (Pants != null)
		{
			var materials = Pants.sharedMaterials;
			materials[0] = Material;
			Pants.sharedMaterials = materials;
		}
	}

	void Update()
	{
		if (!Application.isPlaying)
		{
			UpdateMaterial();
			return;
		}
    
        UpdateAnimation();
		
		if (!isOwned)
	        UpdatePosition();

	    this.Speed = rigidbody.velocity.magnitude;
        UpdateSelection();

		if (Hand != null)
			UpdateSelectionFeedback();
		
		if (!isOwned)
            return;

	    var moveX = Input.GetAxis(MoveXAxis);
	    var moveZ = Input.GetAxis(MoveZAxis);
	    var lookX = Input.GetAxis(LookXAxis);
	    var lookZ = Input.GetAxis(LookZAxis);

	    MoveInput = new Vector3(moveX, 0, moveZ);
	    LookInput = new Vector3(lookX, 0, -lookZ);
	    
        var actionHeld = Input.GetAxis(ActionButton) > 0;
		var actionTriggered = Input.GetButtonDown(ActionButton);
        var toggleLight = Input.GetButtonDown(ToggleLightButton);

		if (actionHeld && Switch != null)
	    {
	        this.ActivateSwitch();
            NetworkManager.Instance.Broadcast(this, "Switch", true);
        }

		if (actionTriggered && Prop != null && Prop.Searchable && !Prop.Searched)
		{
			this.ActivateProp();
			NetworkManager.Instance.Broadcast(this, "Prop", true);
		}

		if (actionTriggered && Safe != null && HasKey)
		{
			this.ActivateSafe();
			NetworkManager.Instance.Broadcast(this, "Safe", true);
		}

	    if (toggleLight && FlashLight != null)
	    {
            bool isOn = !this.FlashLight.activeSelf;
            this.ToggleFlashlight(isOn);
            NetworkManager.Instance.Broadcast(this, "FlashLight", isOn);
	    }

		this.pushing = actionHeld;
	}

    private void UpdatePosition()
    {
        this.transform.position = Utils.Damp(this.transform.position, targetPos, 0.01f, (float)this.deltaTime);
        this.transform.eulerAngles = Utils.Damp(this.transform.eulerAngles, targetOri, 0.01f, (float)this.deltaTime);
    }

    void UpdateSelection()
	{
		float minDistance = float.MaxValue;
		GameObject bestSelection = null;
		var basePosition = transform.position + Vector3.up * BaseHeight;
		var colliders = Physics.OverlapSphere(basePosition, SelectionRadius, Layers.InteractableLayerMask);
		for (int i = 0; i < colliders.Length; ++i)
		{
			var collider = colliders[i];
			var raycastOffset = collider.transform.position - basePosition;
			var offset = raycastOffset;
			offset.y = 0;
			var distance = offset.magnitude;
			var direction = offset.normalized;

			var prop = collider.GetComponentInParent<Prop>();
			if (prop != null && (!prop.Searchable || prop.Searched))
				continue;

			if (Vector3.Angle(transform.forward, offset) > SelectionAngle)
				continue;

			RaycastHit hit;
			if (Physics.Raycast(basePosition, raycastOffset.normalized, out hit, raycastOffset.magnitude,
	                            Layers.InteractableLayerMask | Layers.DefaultLayerMask))
		    {
			    if (hit.collider != collider)
					continue;
			}
			
			if (distance < minDistance)
			{
				minDistance = distance;
				bestSelection = collider.gameObject;
			}
		}

		if (bestSelection != null)
		{
			Switch = bestSelection.GetComponentInParent<Switch>();
			Prop = bestSelection.GetComponentInParent<Prop>();
			Safe = bestSelection.GetComponentInParent<Safe>();
		}
		else
		{
			Switch = null;
			Prop = null;
			Safe = null;
		}
		Selection = bestSelection;
	}

    private void ToggleFlashlight(bool isOn)
    {
        this.FlashLight.SetActive(isOn);
    }

    private void ActivateSwitch()
    {
        if (this.Switch != null)
            this.Switch.Pressed = true;
    }

	void ActivateProp()
	{
		if (Prop != null && Prop.Searchable && !Prop.Searched)
		{
			Prop.Searched = true;
			if (Prop.HasKey)
			{
				HasKey = true;
				Prop.HasKey = false;
			}
		}
	}

	void ActivateSafe()
	{
		if (Safe != null && Safe.Searchable && !Safe.Searched && HasKey)
		{
			Safe.Searched = true;
			HasDiamonds = true;
		}
	}

    void UpdateSelectionFeedback()
	{
		if (Selection == null)
		{
			if (Hand.activeSelf)
				Hand.SetActive(false);
		}
		else
		{
			if (!Hand.activeSelf)
				Hand.SetActive(true);
			var basePosition = transform.position + Vector3.up * BaseHeight;
			Hand.transform.position = Selection.transform.position -
				(Selection.transform.position - basePosition).normalized * HandOffset;
		}
	}
	
	void FixedUpdate()
	{
		var dt = Time.deltaTime;
		var rigidbody = this.rigidbody;
		var basePosition = transform.position + Vector3.up * BaseHeight;

		var forward = Camera == null ? transform.forward : Camera.transform.forward;
		forward.y = 0;
		var inputRotation = Quaternion.LookRotation(forward);

		var targetVelocity = inputRotation * MoveInput * MoveSpeed * (this.pushing ? 0 : 1);
		var velocityChange = targetVelocity - rigidbody.velocity;
		velocityChange.y = 0;
		if (velocityChange.magnitude > MaxVelocityChange * dt)
			velocityChange = velocityChange.normalized * MaxVelocityChange * dt;
		rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);

	    this.deltaTime += dt;
	    if (this.isOwned && this.deltaTime > 0.005f)
	    {
            NetworkManager.Instance.Broadcast(this, "Position", new Location(transform.position, transform.rotation, this.Speed, pushing));
	        this.deltaTime = 0f;
	    }

	    var lookVector = LookInput;
		if (MouseLook && lookVector.magnitude < float.Epsilon)
		{
			var ray = Camera.ScreenPointToRay(Input.mousePosition);
			var groundPosition = ray.origin + ray.direction * (basePosition.y - ray.origin.y) / ray.direction.y;
			lookVector = groundPosition - transform.position;
		}

		if (rigidbody.velocity.magnitude > VelocityThreshold)
		{
			var direction = rigidbody.velocity;
			direction.y = 0;
			var targetLookRotation = Quaternion.LookRotation(direction);
			transform.localRotation = Utils.Damp(transform.localRotation, targetLookRotation, LookDamping, dt);
		}
		else if (this.pushing)
		{
			if (Selection != null)
			{
				var direction = Selection.transform.position - transform.position;
				direction.y = 0;
				var targetLookRotation = Quaternion.LookRotation(direction);
				transform.localRotation = Utils.Damp(transform.localRotation, targetLookRotation, LookDamping, dt);

				var targetPosition = Selection.transform.position - direction.normalized * SnapDistance;
				targetPosition.y = transform.position.y;
				transform.position = Utils.Damp(transform.position, targetPosition, SnapDamping, dt);
			}
		}
		else if (targetVelocity.magnitude > float.Epsilon)
		{
			var targetLookRotation = Quaternion.LookRotation(targetVelocity);
			transform.localRotation = Utils.Damp(transform.localRotation, targetLookRotation, LookDamping, dt);
		}
		else if (lookVector.magnitude > float.Epsilon)
		{
			var targetLookRotation = Quaternion.LookRotation(inputRotation * lookVector);
			transform.localRotation = Utils.Damp(transform.localRotation, targetLookRotation, LookDamping, dt);
		}

	}

    public int NetworkID { get { return this.uniqueID; } set { this.uniqueID = value; } }

    public bool IsOwned
    {
        get { return isOwned; }
        set
        {
            if (!this.isOwned)
                NetworkManager.Instance.UnRegister(this);

            isOwned = value;
            if (!this.isOwned)
                NetworkManager.Instance.Register(this);
        }
    }

    public int ControllerID
    {
        get { return this.controllerID; }
        set
        {
            this.controllerID = value;
            this.UpdateInputControls();
        }
    }

    public bool Pushing
    {
        get { return pushing; }
        set
        {
            if (value == pushing)
                return;

            NetworkManager.Instance.Broadcast(this, "Pushing", Pushing);

            pushing = value;
        }
    }

    public void OnReceived(NetworkPackage inPackage)
    {
        switch (inPackage.Comment)
        {
            case "Position":
                var data = (Location)inPackage.Data;
                this.targetPos = new Vector3(data.PosX, data.PosY, data.PosZ);
                this.targetOri = new Quaternion(data.OriX, data.OriY, data.OriZ, data.OriW).eulerAngles;
                this.pushing = data.Pushing;
                this.Speed = data.Speed;

                break;

            case "Switch":
                this.ActivateSwitch();
                break;

            case "FlashLight":
                this.ToggleFlashlight((bool)inPackage.Data);
                break;
			
			case "Prop":
				this.ActivateProp();
				break;
				
			case "Safe":
				this.ActivateSafe();
				break;

			case "Pushing":
				this.pushing = (bool) inPackage.Data;
				break;
		}
    }
}