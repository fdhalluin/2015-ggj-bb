﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Switch : MonoBehaviour
{
	public Door Door;

	public bool Pressed;
	public bool DefaultPressed = false;
	public bool IsAlarm = true;
	
	public float ResetDelay = 0.5f;
	public float ResetTime = 0;

	public GameObject Button;
	public float ButtonOffset = 0.2f;
	public float ButtonDamping = 0.2f;

	public Material Material;
	public List<MeshRenderer> Renderers = new List<MeshRenderer>();
	public LineRenderer Line;
	public float DoorOffset = 3f;
	public float SwitchOffset = 1f;
	public float EdgeOffset = 0.5f;
	public Light Light;

	private Material _lastMaterial;

	private Vector3 _lastPosition;
	private Vector3 _lastDoorPosition;

	public void Awake()
	{
		UpdateLine();
	}

	public void Update()
	{
		var dt = Time.deltaTime;

		if (Door == null)
		{
			return;
		}
		else if (!Application.isPlaying)
		{
			if (!Door.Switches.Contains(this))
				Door.Switches.Add(this);
		}

		if (Pressed)
			ResetTime = Time.time + ResetDelay;

		if (Application.isPlaying)
			Door.Open |= Time.time <= ResetTime + float.Epsilon;
		else
			Door.Open |= Pressed;

		if (Button != null)
		{
			if (dt < float.Epsilon)
			{
				Button.transform.localPosition = Pressed ? Vector3.forward * ButtonOffset : Vector3.zero;
			}
			else
			{
				var targetPosition = Time.time < ResetTime ? Vector3.forward * ButtonOffset : Vector3.zero;
				Button.transform.localPosition = Utils.Damp(Button.transform.localPosition,
			    	                                        targetPosition, ButtonDamping, dt);
			}
		}

		if (Application.isPlaying)
			Pressed = DefaultPressed;

		if (!Application.isPlaying)
		{
			UpdateLine();
			UpdateMaterial();
		}
	}

	public void UpdateLine()
	{
		_lastPosition = transform.position;
		_lastDoorPosition = Door.transform.position;
		
		var y = _lastPosition.y;
		var startPoint = _lastPosition;
		startPoint.y = y;
		var switchOutPoint = startPoint + transform.rotation * Vector3.back * SwitchOffset;
		var endPoint = _lastDoorPosition;
		endPoint.y = y;
		var sign = Mathf.Sign(Vector3.Dot(Door.transform.forward, switchOutPoint - Door.transform.position));
		var doorOutPoint = endPoint + Door.transform.forward * DoorOffset * sign;
		var doorDirection = Door.transform.rotation * Vector3.forward;
		var midPoint = Mathf.Abs(doorDirection.z) > Mathf.Abs(doorDirection.x)
			? new Vector3(switchOutPoint.x, y, doorOutPoint.z)
				: new Vector3(doorOutPoint.x, y, switchOutPoint.z);

		var startOffset = Mathf.Clamp01(EdgeOffset / (switchOutPoint - startPoint).magnitude);
		var switchOffset = Mathf.Clamp01(EdgeOffset / (midPoint - switchOutPoint).magnitude);
		var doorOffset = Mathf.Clamp01(EdgeOffset / (doorOutPoint - midPoint).magnitude);
		var endOffset = Mathf.Clamp01(EdgeOffset / (endPoint - doorOutPoint).magnitude);

		Line.SetVertexCount(8);
		Line.SetPosition(0, startPoint);
		Line.SetPosition(1, Vector3.Lerp(startPoint, switchOutPoint, 1 - startOffset));
		Line.SetPosition(2, Vector3.Lerp(switchOutPoint, midPoint, switchOffset));
		Line.SetPosition(3, Vector3.Lerp(switchOutPoint, midPoint, 1 - switchOffset));
		Line.SetPosition(4, Vector3.Lerp(midPoint, doorOutPoint, doorOffset));
		Line.SetPosition(5, Vector3.Lerp(midPoint, doorOutPoint, 1 - doorOffset));
		Line.SetPosition(6, Vector3.Lerp(doorOutPoint, endPoint, endOffset));
		Line.SetPosition(7, endPoint);
	}

	public void UpdateMaterial()
	{
		_lastMaterial = Material;
		for (int i = 0; i < Renderers.Count; ++i)
		{
			Renderers[i].sharedMaterial = Material;
		}
		if (Light != null)
		{
			Light.color = Material.color;
		}
		if (Line != null)
		{
			Line.SetColors(Material.color, Material.color);
		}
	}
}
