﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class Room : MonoBehaviour
{
	public Tile TileTemplate;
	public List<Tile> Tiles = new List<Tile>();

	public int Width = 30;
	public int Height = 30;

	public bool AskReset = false;

	public Vector3 LocalRotation;
	public float LocalScale;
	public float Offset;

	public void Awake()
	{
		AskReset = false;
	}

	public void Start()
	{
		AskReset = false;
	}

	public void Update()
	{
		if (AskReset)
		{
			AskReset = false;
			Reset();
		}
	}

	public void Reset()
	{
		var tiles = GetComponentsInChildren<Tile>();
		for (int i = 0; i < tiles.Length; ++i)
		{
			DestroyImmediate(tiles[i].gameObject);
		}
		Tiles.Clear();

		for (int j = 0; j < Height; ++j)
		{
			for (int i = 0; i < Width; ++i)
			{
#if UNITY_EDITOR
				var tileObject = PrefabUtility.InstantiatePrefab(TileTemplate.gameObject) as GameObject;
#else
				var tileObject = Instantiate(TileTemplate.gameObject) as GameObject;
#endif
				var tileTransform = tileObject.transform;
				tileObject.name = TileTemplate.name + "_" + i + "_" + j;
				tileTransform.parent = this.transform;
				tileTransform.localScale = Vector3.one * LocalScale;
				tileTransform.localRotation	= Quaternion.Euler(LocalRotation.x, LocalRotation.y, LocalRotation.z);
				tileTransform.localPosition = new Vector3(i + 0.5f, 0, j + 0.5f) * Offset;
				var tile = tileObject.GetComponent<Tile>();
				tile.X = i;
				tile.Y = j;
				Tiles.Add(tile);
			}
		}
	}
}
