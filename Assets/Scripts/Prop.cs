﻿using UnityEngine;
using System.Collections;

public class Prop : MonoBehaviour
{
	public bool HasKey = false;
	public bool Searched = false;
	public bool Searchable = true;

	public ParticleSystem Particles;

	private bool _lastSearched = false;

	void Start()
	{
		if (Searchable)
		{
			if (Particles != null)
				Particles.Play();
		}
	}

	void Update()
	{
		if (Searchable && Searched != _lastSearched)
		{
			_lastSearched = Searched;
			if (Particles != null)
			{
				if (Searched)
					Particles.Stop();
				else
					Particles.Play();
			}
		}
	}
}
