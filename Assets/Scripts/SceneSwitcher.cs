﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class SceneSwitcher : MonoBehaviour
{
	public List<string> Scenes = new List<string>();

	public Text LevelName;

	void Start()
	{
		if (LevelName != null)
			LevelName.text = Application.loadedLevelName;
	}

	void Update()
	{
		for (var i = KeyCode.Alpha1; i <= KeyCode.Alpha9; ++i)
		{
			int sceneId = (int) i - (int) KeyCode.Alpha1;
			if (sceneId < Scenes.Count && Input.GetKeyDown(i))
				Application.LoadLevel(Scenes[sceneId]);
		}

		if (Input.GetKeyDown(KeyCode.Alpha0))
			Reload();
	}

	public void Reload()
	{
		Application.LoadLevel(Application.loadedLevelName);
	}

	public void Next()
	{
		int loaded = Scenes.IndexOf(Application.loadedLevelName);
		Application.LoadLevel(Scenes[(loaded + 1) % Scenes.Count]);
	}

	public void Previous()
	{
		int loaded = Scenes.IndexOf(Application.loadedLevelName);
		Application.LoadLevel(Scenes[(loaded - 1 + Scenes.Count) % Scenes.Count]);
	}

}
