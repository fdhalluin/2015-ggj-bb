﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PlayerCamera : MonoBehaviour
{
	public Player Player;

	public Vector3 Offset = new Vector3(0, 10, -5);
	public Vector3 LookAt = new Vector3(0, 2, 0);

	public Vector3 BaseRotation = Vector3.zero;

	public Vector3 RefPosition = Vector3.zero;
	public float RefDamping = 0.1f;
	public float RefJitter = 2f;

	public bool AlignWithPlayer = false;

	public float BaseFOV = 25;
	public TopCamera TopCamera;
	public float TransitionDuration = 3;
	public float TransitionPow = 3f;
	private float _transitionProgress = 0;
	private bool _transitioning = true;

	// Use this for initialization
	void Start()
	{
		if (Player == null)
		{
			Player = FindObjectOfType<Player>();
		}
		Player.Camera = this.camera;

		if (TopCamera == null)
			TopCamera = FindObjectOfType<TopCamera>();
	}

	void Update()
	{
		// Update in editor
		if (!Application.isPlaying)
		{
			ResetPosition();
		}
	}

	void ResetPosition()
	{
		var targetRefPosition = Player.transform.position;
		RefPosition = targetRefPosition;
		var forward = Player.transform.forward;
		forward.y = 0;
		var playerRotation = Quaternion.LookRotation(forward);
		var refRotation = AlignWithPlayer ? playerRotation : Quaternion.Euler(BaseRotation.x, BaseRotation.y, BaseRotation.z);
		transform.position = RefPosition + refRotation * Offset;
		transform.LookAt (RefPosition + refRotation * LookAt);
	}
	
	// Update is called once per frame
	void FixedUpdate()
	{
		var dt = Time.deltaTime;
		var targetRefPosition = Player.transform.position;
		var targetOffset = (targetRefPosition - RefPosition);
		var targetDistance = targetOffset.magnitude;
		if (targetDistance >= RefJitter)
		{
			var dampedTarget = RefPosition + targetOffset.normalized * (targetDistance - RefJitter);

			if (dt > float.Epsilon)
				RefPosition = Utils.Damp(RefPosition, dampedTarget, RefDamping, dt);
			else
				RefPosition = dampedTarget;
		}

		var forward = Player.transform.forward;
		forward.y = 0;
		var playerRotation = Quaternion.LookRotation(forward);
		var refRotation = AlignWithPlayer ? playerRotation : Quaternion.Euler(BaseRotation.x, BaseRotation.y, BaseRotation.z);

		if (_transitioning)
		{
			_transitionProgress += dt / TransitionDuration;
			if (_transitionProgress > 1)
				_transitioning = false;
			_transitionProgress = Mathf.Clamp01(_transitionProgress);

			float progressLerp = Mathf.Pow(_transitionProgress, TransitionPow);

			var position = Vector3.Lerp(TopCamera.transform.position, RefPosition + refRotation * Offset, progressLerp);
			var lookAt = Vector3.Lerp(TopCamera.LookAtPosition, RefPosition + refRotation * LookAt, progressLerp);
			camera.fieldOfView = Mathf.Lerp(TopCamera.camera.fieldOfView, BaseFOV, progressLerp);

			transform.position = position;
			transform.LookAt(lookAt);
		}
		else
		{
			transform.position = RefPosition + refRotation * Offset;
			transform.LookAt(RefPosition + refRotation * LookAt);
		}
	}
}
