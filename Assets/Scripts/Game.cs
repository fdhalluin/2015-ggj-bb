﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
	public Player[] Players;
	public ExitZone[] ExitZones;

	public float Duration = 300f;
	public float TimeLeft = 300f;
	public float StartTime;
	public float CurrentTime;

	public bool Running = false;
	public bool Won = false;

	public int EscapedCount = 0;

	public GameObject GameUI;
	public GameObject VictoryScreen;
	public GameObject DefeatScreen;

	public Text CurrentTimeText;
	public Text VictoryTimeText;
	public Text DefeatTimeText;

	void Start()
	{
		Reset();
	}

	void Reset()
	{
		Players = FindObjectsOfType<Player>();
		ExitZones = FindObjectsOfType<ExitZone>();
	
		StartTime = Time.time;
		TimeLeft = Duration;
		EscapedCount = 0;
		Running = true;

		if (GameUI != null)
			GameUI.SetActive(true);
		if (DefeatScreen != null)
			DefeatScreen.SetActive(false);
		if (VictoryScreen != null)
			VictoryScreen.SetActive(false);
	}

	void Update()
	{
		if (!Running)
			return;

		CurrentTime = Time.time - StartTime;
		TimeLeft = Duration - CurrentTime;

		if (TimeLeft < 0)
		{
			Lose();
			TimeLeft = 0;
		}
		
		if (CurrentTimeText != null)
			CurrentTimeText.text = string.Format("{0:0}:{1:00}", Mathf.Floor(TimeLeft / 60), Mathf.Ceil(TimeLeft % 60));

		CheckExit();
	}

	void CheckExit()
	{
		EscapedCount = 0;
		for (int i = 0; i < Players.Length; ++i)
		{
			var player = Players[i];
			player.HasEscaped = false;
			for (int j = 0; j < ExitZones.Length; ++j)
			{
				var zone = ExitZones[j];
				var bounds = zone.collider.bounds;
				if (bounds.Contains(player.transform.position + Vector3.up))
				{
					player.HasEscaped = true;
					++EscapedCount;
					break;
				}
			}
		}

		if (EscapedCount == Players.Length)
			Win();
	}

	void Lose()
	{
		Running = false;

		if (DefeatScreen != null)
			DefeatScreen.SetActive(true);
		if (DefeatTimeText != null)
			DefeatTimeText.text = string.Format("{0:0}:{1:00}", Mathf.Floor(CurrentTime / 60), Mathf.Floor(CurrentTime % 60));
		Won = false;
	}

	void Win()
	{
		Running = false;

		if (VictoryScreen != null)
			VictoryScreen.SetActive(true);
		if (VictoryTimeText != null)
			VictoryTimeText.text = string.Format("{0:0}:{1:00}", Mathf.Floor(CurrentTime / 60), Mathf.Floor(CurrentTime % 60));
		Won = true;
	}
}
