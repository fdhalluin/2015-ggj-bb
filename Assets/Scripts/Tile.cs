using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Tile : MonoBehaviour
{
	public int X;
	public int Y;

	public Mesh Mesh;
	public MeshFilter MeshFilter;
	public MeshCollider MeshCollider;

	private Mesh _lastMesh;

	public void Start()
	{
		if (Application.isPlaying)
			DestroyImmediate(this);
	}

	public void Update()
	{
		if (Application.isPlaying)
			return;

		if (MeshFilter == null)
			MeshFilter = GetComponent<MeshFilter>();

		if (MeshCollider == null)
			MeshCollider = GetComponent<MeshCollider>();

		if (Mesh != _lastMesh)
		{
			_lastMesh = Mesh;
			if (MeshFilter != null)
				MeshFilter.sharedMesh = Mesh;
			if (MeshCollider != null)
				MeshCollider.sharedMesh = Mesh;
		}
	}
}
