﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TopCamera : MonoBehaviour
{
	public Vector3 LookAtPosition = Vector3.zero;
	
	public float Zoom = 5;
	public float ZoomSpeed = 3;
	public float MinZoom = 1;
	public float MaxZoom = 10;
	
	public float ZoomOrthographicSizeFactor = 1;
	public float ZoomDistanceFactor = 10;
	
	public float RotatePitchFactor = 3;
	public float RotateYawFactor = 3;
	
	private Vector2 _lastMousePosition;
	private Vector2 _pressedMousePosition;
	private Vector3 _pressedGroundPosition;
	private Vector3 _pressedLookAtPosition;

	// Update is called once per frame
	void Update()
	{
		var camera = this.camera;

		Vector2 mouseDelta = (Vector2) Input.mousePosition - _lastMousePosition;
		_lastMousePosition = (Vector2) Input.mousePosition;
		var mouseScrollWheel = Input.mouseScrollDelta.y;
		
		if (Mathf.Abs(mouseScrollWheel) > float.Epsilon)
		{
			Zoom = Mathf.Clamp(Zoom + ZoomSpeed * -mouseScrollWheel, MinZoom, MaxZoom);
		}
		
		if (Input.GetMouseButtonDown(0))
		{
			_pressedMousePosition = camera.ScreenToViewportPoint(Input.mousePosition);
			var ray = camera.ViewportPointToRay(_pressedMousePosition);
			_pressedGroundPosition = ray.origin + ray.direction * -ray.origin.y / ray.direction.y;
			_pressedLookAtPosition = LookAtPosition;
		}
		
		if (Input.GetMouseButton(0))
		{
			var oldRay = camera.ViewportPointToRay(_pressedMousePosition);
			var oldGroundPosition = oldRay.origin + oldRay.direction * -oldRay.origin.y / oldRay.direction.y;
			var newRay = camera.ViewportPointToRay(camera.ScreenToViewportPoint(Input.mousePosition));
			var newGroundPosition = newRay.origin + newRay.direction * -newRay.origin.y / newRay.direction.y;
			var offset = newGroundPosition - oldGroundPosition;
			LookAtPosition = _pressedLookAtPosition - offset;
		}
		
		if (Input.GetMouseButton(1))
		{
			float refScreenSize = Mathf.Min (Screen.width, Screen.height);
			float mouseX = mouseDelta.x / refScreenSize;
			float mouseY = mouseDelta.y / refScreenSize;
			
			var euler = transform.localEulerAngles;
			euler += new Vector3(mouseY * RotatePitchFactor, mouseX * RotateYawFactor, 0);
			euler.x = Mathf.Clamp(euler.x, 5f, 85f);
			transform.localEulerAngles = euler;
		}

		transform.position = LookAtPosition - transform.localRotation * Vector3.forward * Zoom * ZoomDistanceFactor;
		camera.orthographicSize = Zoom * ZoomOrthographicSizeFactor;
	}

	public void OnPreCull()
	{
		var lights = FindObjectsOfType<Light>();
		for (int i = 0; i < lights.Length; ++i)
		{
			var light = lights[i];
			var layer = light.gameObject.layer;
			light.enabled |= layer == Layers.TopViewOnlyLayer;
			light.enabled &= layer != Layers.PlayerViewOnlyLayer;
		}
	}
	
	public void OnPostRender()
	{
		var lights = FindObjectsOfType<Light>();
		for (int i = 0; i < lights.Length; ++i)
		{
			var light = lights[i];
			var layer = light.gameObject.layer;
			light.enabled &= layer != Layers.TopViewOnlyLayer;
			light.enabled |= layer == Layers.PlayerViewOnlyLayer;
		}
	}
}
