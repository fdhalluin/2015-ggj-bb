﻿using UnityEngine;
using UnityEngine.UI;

public class UIStartup : MonoBehaviour
{
    public GameObject IPSelection;
    public GameObject Background;
    public GameObject Header;


    public void OnCountClicked(int count)
    {
        PlayerManager.Instance.TotalPlayerCount = count;
    }

    public void OnLocalCountClicked(int count)
    {
        PlayerManager.Instance.LocalPlayerCount = count;
    }


    public void OnObserverHereClicked(bool yes)
    {
        PlayerManager.Instance.ShowObserverMap = yes;
    }

    public void OnObserverHereClicked(GameObject ipSelection)
    {
        if (PlayerManager.Instance.LocalPlayerCount == PlayerManager.Instance.TotalPlayerCount)
            InitPlayers();
        else
            ipSelection.SetActive(true);
    }

    
    public void OnIPSetClicked(Text ip)
    {
        NetworkManager.Instance.ServerIP = ip.text;

        if (NetworkManager.Instance != null && PlayerManager.Instance != null
            && PlayerManager.Instance.LocalPlayerCount < PlayerManager.Instance.TotalPlayerCount)
        {
            if (!NetworkManager.Instance.Init())
                return;

            InitPlayers();
        }
    }

    private void InitPlayers()
    {
        IPSelection.SetActive(false);
        Background.SetActive(false);
        Header.SetActive(false);

        PlayerManager.Instance.Init();
    }
}
