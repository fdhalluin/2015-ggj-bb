﻿Shader "Custom/SwitchLine" 
{
  Properties
  {
    _Color ("Main Color", Color) = (1,1,1,1)
  }
  SubShader
  {
    Tags { "Queue" = "Overlay-1000" } 
    // draw after all opaque geometry has been drawn

	BindChannels { Bind "vertex", vertex Bind "color", color }

	Pass
    { 
      Cull Off // Draw front and back faces
      ZWrite Off // Don't write to depth buffer
      // in order not to occlude other objects
      ZTest Always

      Blend SrcAlpha One // Additive blending

      CGPROGRAM 
        #pragma vertex vert 
        #pragma fragment frag

        fixed4 _Color;
        
		struct appdata_t
		{
			float4 vertex : POSITION;
			fixed4 color : COLOR;
		};
        
		struct v2f
		{
			float4 vertex : POSITION;
			fixed4 color : COLOR;
		};

		v2f vert(appdata_t v)
		{
			v2f o;
			o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
			o.color = v.color;
			return o;
		}

        float4 frag(v2f i) : COLOR {
          return i.color * _Color;
        }
      ENDCG  
    }
  }
}