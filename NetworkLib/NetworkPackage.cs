using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace NetworkLib
{
    [Serializable]
    public struct NetworkPackage
    {
        public long Timestamp { get; set; }
        public int NetworkID { get; set; }
        public string Comment { get; set; }
        public object Data { get; set; }

        // Convert an object to a byte array
        public byte[] ObjectToByteArray()
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, this);
            return ms.ToArray();
        }

        public static byte[] BuildHeaderPackage(NetworkPackage networkPackage)
        {
            byte[] objectToByteArray = networkPackage.ObjectToByteArray();
            byte[] data = new byte[objectToByteArray.Length + 4];

            Buffer.BlockCopy(objectToByteArray, 0, data, 4, objectToByteArray.Length);
            byte[] header = BitConverter.GetBytes(objectToByteArray.Length);
            Buffer.BlockCopy(header, 0, data, 0, header.Length);
            return data;
        }

        public static byte[] BuildHeaderPackage(byte[] networkPackage)
        {
            byte[] objectToByteArray = networkPackage;
            byte[] data = new byte[objectToByteArray.Length + 4];

            Buffer.BlockCopy(objectToByteArray, 0, data, 4, objectToByteArray.Length);
            byte[] header = BitConverter.GetBytes(objectToByteArray.Length);
            Buffer.BlockCopy(header, 0, data, 0, header.Length);
            return data;
        }

        public static NetworkPackage GetPackage(byte[] data)
        {
            IFormatter formatter = new BinaryFormatter();
            var stream = new MemoryStream(data);
            NetworkPackage o = (NetworkPackage)formatter.Deserialize(stream);
            
            return o;
        }

        public override string ToString()
        {
            return string.Format("NetworkID: {0}, Comment: {1}, Data: {2}", this.NetworkID, this.Comment, this.Data);
        }
    }
}