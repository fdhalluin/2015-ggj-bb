local _map, _rooms, _doors


-- unrotated tiles
local Tiles = {
    { 0, 0, 0, 0, 0, 0, 0, 0, },
    { 0, 0, 1, 0, 0, 0, 0, 0, },
    { 1, 0, 1, 0, 0, 0, 0, 0, },
    { 1, 0, 1, 0, 0, 0, 1, 0, },
    { 1, 0, 1, 0, 1, 0, 1, 0, },
    { 0, 0, 1, 0, 0, 0, 1, 0, },

    { 1, 1, 1, 0, 0, 0, 0, 0, }, -- 7
    { 1, 0, 0, 0, 1, 1, 1, 0, }, -- 10
    { 1, 0, 1, 0, 1, 1, 1, 0, }, -- 9
    { 0, 0, 1, 0, 1, 1, 1, 0, }, -- 8
    { 1, 1, 1, 0, 1, 1, 1, 0, }, -- 11
    { 1, 1, 1, 1, 1, 0, 0, 0, }, -- 12
    { 1, 1, 1, 0, 1, 1, 1, 1, }, -- 13
    { 1, 1, 1, 0, 1, 0, 1, 1, }, -- 14

    { 1, 1, 0, 0, 0, 1, 1, 0, }, -- map 15 to 11
    { 1, 1, 1, 0, 0, 1, 1, 0, }, -- map 16 to 11
    { 1, 1, 0, 0, 1, 1, 1, 0, }, -- map 17 to 11

    -- { 1, 1, 1, 1, 1, 1, 1, 1, }, -- 15 blocked
    -- { 1, 1, 1, 1, 1, 1, 1, 1, }, -- 16 pillar
}

local TileHash = {}
for index, tile in ipairs(Tiles) do
    for r = 0, 3 do
        local hash = 0
        for i, p in ipairs(tile) do
            hash = hash + p * math.pow(2, ((i+r*2)-1)%8)
        end
        if not TileHash[hash] then
            TileHash[hash] = {
                index = index,
                rotation = r * 90
            }
        end
    end
end

function hasDoor(x1, y1, x2, y2)
    -- super slow...
    for _, door in ipairs(_doors) do
        if door.x1 == x1
           and door.y1 == y1
           and door.x2 == x2
           and door.y2 == y2 then
           return true
        elseif door.x1 == x2
           and door.y1 == y2
           and door.x2 == x1
           and door.y2 == y1 then
           return true
        end

    end
end

function isBlocked(x1, y1, x2, y2)
    indexA = _map[x1][y1] or 0
    indexB = _map[x2][y2] or 0
    
    if indexA == indexB then
        return 0
    end

    if hasDoor(x1, y1, x2, y2) then
        return 0
    end

    return 1
end

function isBlockedCorner(indexA, indexB)
    indexA = indexA or 0
    indexB = indexB or 0

    if indexA == indexB then
        return 0
    end

    return 1
end

function getTile(map, x, y)
    if map[x][y] == 0 then
        return 15, math.random(4) * 90
    end

    local index = map[x][y]
    local a1, a2, a3 = 1, 1, 1
    local b1, b2, b3 = 1, 1, 1
    local c1, c2, c3 = 1, 1, 1

    if map[x-1] then
        a1 = isBlockedCorner(map[x-1][y-1], index)
        b1 = isBlocked(x, y, x-1, y)
        c1 = isBlockedCorner(map[x-1][y+1], index)
    end

    a2 = isBlocked(x, y, x, y-1)
    b2 = 0
    c2 = isBlocked(x, y, x, y+1)

    if map[x+1] then
        a3 = isBlockedCorner(map[x+1][y-1], index)
        b3 = isBlocked(x, y, x+1, y)
        c3 = isBlockedCorner(map[x+1][y+1], index)
    end

    -- corner cases
    a1 = (a1 == 0 and b1 == 0 and a2 == 0) and 0 or 1
    a3 = (a3 == 0 and b3 == 0 and a2 == 0) and 0 or 1
    c1 = (c1 == 0 and b1 == 0 and c2 == 0) and 0 or 1
    c3 = (c3 == 0 and b3 == 0 and c2 == 0) and 0 or 1

    local hash = 0
    local values = { a1, a2, a3, b3, c3, c2, c1, b1 }
    for i, v in ipairs(values) do
        hash = hash + v * math.pow(2, (i-1))
    end

    local tile = TileHash[hash] or TileHash[0]

    -- if x == 13 and y == 9 then
    --     local door = hasDoor(x, y, x, y-1)
    --     print(tostring(door))
    --     print(a1..a2..a3.. "\n" .. b1..b2..b3.. "\n" .. c1..c2..c3.. "\n\n")
    -- end

    -- Did anybody say "Hack"?!
    if tile.index >= 15 and tile.index <= 17 then
        return 11, tile.rotation, tostring(hash)
    end

    return tile.index, tile.rotation, tostring(hash)
end

local Type = {
    ["tile"] = 1,
    ["prop"] = 2,
}

local PropId = {
    ["door"] = 0,
    ["table"] = 10,
}

function export(filename, map, rooms, doors, props)
    local file = io.open(filename, "w")
    _map = map
    _rooms = rooms
    _doors = doors
    io.output(file)
    for y = 1, #map[1] do
        for x = 1, #map do
            local tile, rotation, hash = getTile(map, x, y)
            io.write(x - 1 .. "\t" .. y - 1 .. "\t" .. (-rotation) .. "\t" .. tile .. "\t" .. Type.tile .. "\n")
        end
    end

    for i, door in ipairs(doors) do
        local x = (door.x1 + door.x2) / 2 - 1
        local y = (door.y1 + door.y2) / 2 - 1
        local r = x1 == x2 and 0 or 90
        io.write(x .. "\t" .. y .. "\t" .. r .. "\t" .. PropId.door .. "\t" .. Type.prop .. "\n")
    end

    for index, room in pairs(rooms) do
        local x = room.x + (room.w) / 2 + 1
        local y = room.y + (room.h) / 2 + 1
        local r = 0
        io.write(x .. "\t" .. y .. "\t" .. r .. "\t" .. PropId.table .. "\t" .. Type.prop .. "\n")
    end

    io.close(file)
end

return {
    export = export,
    getTile = getTile,
}