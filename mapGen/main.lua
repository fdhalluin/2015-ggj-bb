local Gfx = love.graphics
local Pickle = require "pickle"
local MapGenerator = require "mapgenerator"
local Exporter = require "exportmap"

local map, rooms, doors
local seed

local Zoom = 32
local ColorPalette = {}
local ColorGray = { 20, 20, 20 }
local ColorWhite = { 255, 255, 255 }
local ColorBlack = { 0, 0, 0 }

ColorPalette[0] = ColorGray
ColorPalette[1] = ColorWhite

function getColor(index)
    if not ColorPalette[index] then
        ColorPalette[index] = {
            math.random(255),
            math.random(255),
            math.random(255),
        }
    end

    return ColorPalette[index]
end

function setColor(color)
    Gfx.setColor(color[1], color[2], color[3])
end

function love.draw()
    for x, column in ipairs(map) do
        for y, item in ipairs(column) do
            local color = item ~= 0 and getColor(item) or ColorGray
            setColor(color)
            Gfx.rectangle("fill", x * Zoom, y * Zoom, Zoom, Zoom)
            Gfx.setColor(255, 0, 0)
            -- local i, r, hash = Exporter.getTile(map, x, y)
            -- Gfx.print(i, x * Zoom, y * Zoom)
            -- Gfx.print(tostring(hash), x * Zoom, y * Zoom + Zoom/2 )

        end
    end

    Gfx.setColor(255, 0, 0)

    -- draw connections
    for indexA, roomA in pairs(rooms) do
        setColor(ColorWhite)
        -- local text = "Room " .. indexA
        -- text = text .. "\n" .. table.concat(roomA.connections, ",")
        -- Gfx.print(text, roomA.x * Zoom, roomA.y * Zoom)
    --[[
        local color = getColor(indexA)
        setColor(color)
        for indexB in pairs(roomA.right) do
            local roomB = rooms[indexB]
            Gfx.line((roomA.w/2+roomA.x) * Zoom, (roomA.h/2+roomA.y) * Zoom,
                     (roomB.w/2+roomB.x) * Zoom, (roomB.h/2+roomB.y) * Zoom)
        end
        for indexB in pairs(roomA.bottom) do
            local roomB = rooms[indexB]
            Gfx.line((roomA.w/2+roomA.x) * Zoom, (roomA.h/2+roomA.y) * Zoom,
                     (roomB.w/2+roomB.x) * Zoom, (roomB.h/2+roomB.y) * Zoom)
        end
    --]]
    end

    -- draw doors
    setColor(ColorBlack)
    for _, door in ipairs(doors) do
        -- print(door.x1, door.x2, door.y1, door.y2)
        Gfx.circle("fill", (door.x1+door.x2+1)/2 * Zoom, (door.y1+door.y2+1)/2 * Zoom, 0.3 * Zoom)
    end

    Gfx.print(seed, 0, 0)
end

function love.keypressed(key, unicode)
    local altPressed = love.keyboard.isDown('lalt') or love.keyboard.isDown('ralt')
    if key == "escape" then
        -- sends quit event
        love.event.quit()
    end
    if key == 'return' and altPressed then
       fullscreen = not fullscreen
       love.window.setFullscreen(fullscreen, "desktop")
    end
    if key == 'r' then
        seed = os.time()
        math.randomseed(seed)
        map, rooms, doors = MapGenerator.createMap()
    end
end

function love.load(arg)
    love.window.setMode(1200, 800, {resizable=true, vsync=false, minwidth=400, minheight=300})
    seed = os.time()
    seed = 123
    math.randomseed(seed)

    map, rooms, doors = MapGenerator.createMap()
    Exporter.export("..\\Assets\\Maps\\test.txt", map, rooms, doors)
end
