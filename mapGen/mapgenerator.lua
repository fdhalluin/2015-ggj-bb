local map
local rooms
local doors
local switches
local props

function generateRoom(min, max)
    local width = math.random(min, max)
    local height = math.random(min, max-width/2)
    return width, height
end

function tryPlaceRoom(map, x, y, w, h, index)
    -- check if this position is still free
    for i = x, x+w-1 do
        for j = y, y+h-1 do
            if map[i][j] > 0 then
                return false
            end
        end
    end

    -- add to the room table
    rooms[index] = {
        x = x,
        y = y,
        w = w,
        h = h,
        -- (potentially) neighboring rooms
        connections = {},
    }

    -- place the new room
    for i = x, x+w-1 do
        for j = y, y+h-1 do
            map[i][j] = index
        end
    end

    return true
end

function tryConnectRoom(map, room)
    local indexA = map[room.x][room.y]

    local connections = {}
    for index in pairs(room.connections) do
        connections[#connections + 1] = index
    end

    -- pick a random room to connect to
    local indexB = connections[math.random(#connections)]
    local roomB = rooms[indexB]
    -- get a list of possible connection tiles
    local tiles = {}
    for x = room.x, room.x + room.w - 1 do
        if x >= roomB.x and x < roomB.x + roomB.w then
            tiles[#tiles + 1] = {
                x = x,
                y = room.y + (room.y < roomB.y and room.h - 1 or 0),
            }
        end
    end
    for y = room.y, room.y + room.h - 1 do
        if y >= roomB.y and y < roomB.y + roomB.h then
            tiles[#tiles + 1] = {
                y = y,
                x = room.x + (room.x < roomB.x and room.w - 1 or 0),
            }
        end
    end


    -- shouldn't actually happen...
    if #tiles == 0 then
        return
    end

    -- get a tile from roomA
    local tile = tiles[math.random(#tiles)]
    local x, y = tile.x, tile.y
    local dirX = x < roomB.x and 1 or x >= roomB.x + roomB.w and -1 or 0
    local dirY = y < roomB.y and 1 or y >= roomB.y + roomB.h and -1 or 0

    print(x, y, x + dirX, y + dirY)
    doors[#doors + 1] = {
        x1 = x,
        y1 = y,
        x2 = x + dirX,
        y2 = y + dirY,
    }

    x = x + dirX
    y = y + dirY

    -- walk in proposed direction until hitting another room
    while map[x][y] < 2 do
        map[x][y] = 1
        x = x + dirX
        y = y + dirY
    end

    doors[#doors + 1] = {
        x1 = x,
        y1 = y,
        x2 = x - dirX,
        y2 = y - dirY,
    }

    -- delete connection
    room.connections[indexB] = nil
    roomB.connections[indexA] = nil
end

function initArray(size)
    local array = {}
    for i = 1, size do
        array[i] = 0
    end
    return array
end

function tryPlaceProps(map, room)
    --
end

function findNeighbors(map)
    local width = #map
    local height = #map[1]

    -- do a sweep on x axis
    local rows = initArray(height)
    for x = 1, width do
        for y = 1, height do
            if map[x][y] ~= rows[y] and map[x][y] ~= 0 then
                local leftRoom = rooms[rows[y]]
                local rightRoom = rooms[map[x][y]]
                if leftRoom then
                    leftRoom.connections[map[x][y]] = true
                end
                if rightRoom and rows[y] ~= 0 then
                    rightRoom.connections[rows[y]] = true
                end
                rows[y] = map[x][y]
            end
        end
    end

    -- do a sweep on y axis
    local columns = initArray(width)
    for y = 1, height do
        for x = 1, width do
            if map[x][y] ~= columns[x] and map[x][y] ~= 0 then
                local topRoom = rooms[columns[x]]
                local bottomRoom = rooms[map[x][y]]
                if topRoom then
                    topRoom.connections[map[x][y]] = true
                end
                if bottomRoom and columns[x] ~= 0 then
                    bottomRoom.connections[columns[x]] = true
                end
                columns[x] = map[x][y]
            end
        end
    end
end

-- init empty map (size in tiles)
function initMap(width, height)
    local map = {}
    for i = 1, width do
        local column = {}
        for j = 1, height do
            column[j] = 0
        end
        map[i] = column
    end

    rooms = {}
    doors = {}
    props = {}

    return map
end

-- width/height in tiles
function simpleLevel(width, height)
    map = initMap(width, height)

    -- place rooms
    local fails = 0
    local numRooms = 0
    local maxSize = 8
    while fails < 55 do
        local w, h = generateRoom(3, maxSize)
        local x = math.random(width-w)
        local y = math.random(height-h)
        local success = tryPlaceRoom(map, x, y, w, h, numRooms + 2)
        if success then
            numRooms = numRooms + 1
        else
            fails = fails + 1
            if maxSize > 3 and (fails % 3 == 0) then
                maxSize = maxSize - math.floor(math.sqrt(maxSize))
            end
        end
    end

    findNeighbors(map)

    for index, room in pairs(rooms) do
        -- create corridors
        tryConnectRoom(map, room)
        -- place props
        tryPlaceProps(map, room)
    end

    print("Placed " .. numRooms .. " rooms on a " .. width .. "x" .. height .. " tile map.")
end

function createMap()
    simpleLevel(35, 25)
    return map, rooms, doors, props
end

return {
    createMap = createMap
}
